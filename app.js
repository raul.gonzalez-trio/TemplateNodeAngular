const express      = require('express'),
      app          = express(),
      bodyParser   = require('body-parser'),
      env          = process.env;

app.disable('x-powered-by');

var mailExp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
var passExp = /^.{8,30}$/;
var dateExp = /[0-9][0-9]\/[0-9][0-9]\/[1-3][0-9][0-9][0-9]/;

app.use(bodyParser.json({type:'application/*',limit:'2mb'}));
app.use(bodyParser.urlencoded({extended:false}));

app.use(function(req, res, next) {
  /*if(req.hostname==="triolabs.mx"){
    res.redirect(301, 'http://www.triolabs.mx' + req.path);
  }else{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, id, key");
    next();
  }*/
});

const api = require('./server/api');
//set our api routes
app.use('/api',api);

app.get("*.js", (req, res, next) => {
    //console.log(`${req.url} -> ${req.url}.gz`);
    req.url = `${req.url}.gz`;
    res.set("Content-Encoding", "gzip");
    res.set("Content-Type", "text/javascript");
    next();
});

app.get("*.css", (req, res, next) => {
    //console.log(`${req.url} -> ${req.url}.gz`);
    req.url = `${req.url}.gz`;
    res.set("Content-Encoding", "gzip");
    res.set("Content-Type", "text/css");
    next();
});

//app.use(express.static('build'));
app.use(express.static(__dirname + '/build'));

app.get('/html/:lang/:name', function(req, res){
  res.sendFile('build/html/'+req.params.lang+'/'+req.params.name+'.html', { root: __dirname });
});

app.all('/*', function(req, res, next) {
  // Just send the index.html for other files to support HTML5Mode
  res.set("Content-Encoding", "gzip");
  res.set("Content-Type", "text/html");
  res.sendFile('build/index.html.gz', { root: __dirname });
});

console.log(env.PORT||9999);

app.listen(env.PORT || 8080, function () {
  console.log(
    '\Application worker started\nPID: %d On: %s:%s...',
    process.pid, (env.NODE_IP ? env.NODE_IP : 'localhost'), (env.PORT ? env.PORT : '8080')
  );
});
