# Template

### Para correrlo local

- Cada cambio que requiera hacer en front end, requiere un npm run build (a menos que ya este funcionando con dev)
- Con node app.js, node start.js, npm start inicia el servidor completo
  - El servidor manda los archivos de /build como estaticos, por eso requiere un build con npm

- Opcional: se puede usar nodemon para que cada cambio en la api se vea reflejado sin reiniciar servidor manualmente
  - El comando es nodemon --ignore build/ app.js
