function OnRun($rootScope, $window, AppSettings) {
  'ngInject';

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.pageTitle = '';
    if (toState.title) {
      $rootScope.pageTitle += toState.title;
    }

    $rootScope.pageTitle += AppSettings.appTitle;
  });
  
}

export default OnRun;
