const AppSettings = {
  appTitle: '',
  apiUrl: '/api/v1'
};

export default AppSettings;
