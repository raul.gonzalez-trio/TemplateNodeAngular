import request from 'request';

function ExampleCtrl($scope, $rootScope, $cookies, $window) {
  'ngInject'

  // ViewModel
  const vm = this;
  vm.urlprfx = $window.location.origin+'/';
  $rootScope.pageTitle = 'Triolabs';
 
}

export default {
  name: 'ExampleCtrl',
  fn: ExampleCtrl
};
