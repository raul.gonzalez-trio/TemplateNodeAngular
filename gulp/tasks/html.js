import config      from '../config';
import changed     from 'gulp-changed';
import gulp        from 'gulp';
import browserSync from 'browser-sync';

gulp.task('html', function() {

  return gulp.src(config.html.src)
    .pipe(changed(config.html.dest)) // Ignore unchanged files
    .pipe(gulp.dest(config.html.dest))
    .pipe(browserSync.stream());

});